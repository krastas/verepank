﻿namespace Verepank.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Verepank.HumanDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Verepank.HumanDbContext";
        }

        protected override void Seed(Verepank.HumanDbContext context)
        {

        }
    }
}
