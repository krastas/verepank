﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verepank.Domain.Enums;

namespace Verepank.Domain
{
    public class HumanBlood
    {
        public int HumanBloodId { get; set; }

        public int PersonId { get; set; }
        public virtual Person Person { get; set; }
        public int Amount { get; set; }
        public virtual Blood Blood { get; set; }
    }
}
