﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verepank.Domain;
using Verepank.Domain.Enums;

namespace Verepank
{
    class Program
    {
        static void Main(string[] args)
        {
            var person = AddPerson("Ants-Peeter", "Pakiraam", "olenlahe@email.com", 131245314);
            var blood = AddBlood(BloodType.A,Reesus.Miinus);
            HumanBlood( person, blood, 5);
        }

        static Person AddPerson(string firstname, string lastname, string email, int phone)
        {
            using (var db = new HumanDbContext())
            {
                var newPerson = new Person()
                {
                    Firstname = firstname,
                    Lastname = lastname,
                    Email = email,
                    Phone = phone
                    
                };

                db.People.Add(newPerson);
                db.SaveChanges();
                return newPerson;
            }

        }

        static Blood AddBlood(BloodType bloodType, Reesus reesus)
        {
            using (var db = new HumanDbContext())
            {
                var newBlood = new Blood()
                {
                    BloodType = bloodType,
                    Reesus = reesus
                };

                db.Bloods.Add(newBlood);
                db.SaveChanges();
                return newBlood;
            }
        }

        static void HumanBlood(Person pers, Blood bloods, int amount)
        {
            using (var db = new HumanDbContext())
            {
                var humanBlood = new HumanBlood()
                {
                    Person = pers,
                    Blood = bloods,
                    Amount = amount
                    
                };
                db.HumanBlood.Add(humanBlood);
                db.SaveChanges();
            }
        }
    }
}
