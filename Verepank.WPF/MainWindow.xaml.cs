﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Verepank.Domain;
using Verepank.Services;
using Verepank.Services.Interfaces;
using Verepank.WPF.ViewModels;

namespace Verepank.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowVM _vm;

        public MainWindow()
        {
            InitializeComponent();
            this.Loaded += MainWindow_Loaded;
        }
        //Peaakna laadimine
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            _vm = new MainWindowVM();
            _vm.LoadData();
            this.DataContext = _vm;
        }
        //Nupu vajutamisel kontrollitakse tühje lahtereid ning nende puudumisel lisatakse uus inimene
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            int value;
            if (!int.TryParse(txtAmount.Text, out value))
            {
                MessageBox.Show(" Palun sisesta õige kogus");
                return;
            }
            if (txtFirstname.Text == "" || txtLastname.Text == "" || txtEmail.Text == "" || txtPhone.Text == "" || txtReesusmiinus.IsChecked == false && txtReesuspluss.IsChecked == false || txtBlood.Text == "" || txtAmount.Text == "")
            {
                MessageBox.Show(" Palun sisesta kõikidesse lahtritesse andmed");
                return;
            }
            else
            {
                MessageBox.Show(" Inimene lisatud!");
            }

            _vm.AddNewPerson(lboxData, txtFirstname, txtLastname, txtEmail, txtPhone,
             txtReesusmiinus, txtnull, txtA, txtAB, txtAmount, btnAdd);
            _vm.LoadData();

            txtFirstname.Clear();
            txtLastname.Clear();
            txtEmail.Clear();
            txtPhone.Clear();
            txtAmount.Clear();
            txtBlood.Items.Clear();
            txtReesusmiinus.IsChecked = false;
            txtReesuspluss.IsChecked = false;
        }
        //Inimese selekteerimisel täidetakse inimese andmed peavaatesse
        private void lboxData_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _vm.FillData(lboxData, txtFirstname, txtLastname, txtEmail, txtPhone, txtBlood,
              txtReesuspluss, txtReesusmiinus, btnAdd);
        }
        //Eesnime kirjutamisel peavaates olevasse eesnime lahtrisse, otsitakse listist inimesi eesnime järgi
        private void txtNameToSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            _vm.FirstNameSearch(lboxData, txtFirstname);

        }
        //Perekonnanime kirjtuamisel peavaates olevasse perekonnanime lahtrisse, otsitakse listist inimesi perekonnanime jörgi
        private void txtNameToSearch_TextChanged2(object sender, TextChangedEventArgs e)
        {
            _vm.LastNameSearch(lboxData, txtLastname);

        }
        //Liikumine statistika akna peale nupu vajutamisel
        private void btnStat_Click(object sender, RoutedEventArgs e)
        {
            StatisticksWindow statisticks = new StatisticksWindow();
            App.Current.MainWindow = statisticks;
            this.Close();
            statisticks.Show();
        }
        //Nupu vajutamisel liigutakse vere eemaldamise akna peale
        private void btnInim_Click_1(object sender, RoutedEventArgs e)
        {
            RemoveBlood rm = new RemoveBlood();
            App.Current.MainWindow = rm;
            this.Close();
            rm.Show();
        }
    }
}
