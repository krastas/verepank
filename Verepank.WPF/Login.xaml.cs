﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Verepank.Services;
using Verepank.Services.Interfaces;
using Verepank.WPF.ViewModels;

namespace Verepank.WPF
{
    /// <summary>
    /// Interaction logic for login.xaml
    /// </summary>
    public partial class login : Window
    {
        private LoginVM _vm;
        public login()
        {
            InitializeComponent();
            this.Loaded += Login_Loaded;
        }
        private void Login_Loaded(object sender, RoutedEventArgs e)
        {
            _vm = new LoginVM();
            this.DataContext = _vm;
        }
        // nuppu vajutades logib sisse kas arsti või doonori vaatesse
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            IPersonService personService = new PersonService(new HumanDbContext());
            var persons = personService.GetAllPersons();
            var user = "";
            var pass = "";
            if (txtUser.Text.Equals("arst") & txtPassword.Password.Equals("arst"))
            {
                MainWindow main = new MainWindow();
                App.Current.MainWindow = main;
                this.Close();
                main.Show();
                return;
            }
            foreach (var item in persons)
            {
                user = item.Firstname;
                pass = item.Lastname;
                if (txtUser.Text.Equals(user) & txtPassword.Password.Equals(pass))
                {

                    user = item.Firstname;
                    pass = item.Lastname;
                    _vm.Login(txtUser, txtPassword);
                    UserStat statisticks = new UserStat(user, pass);
                    App.Current.MainWindow = statisticks;
                    this.Close();
                    statisticks.Show();
                    return;
                }
            }
            MessageBox.Show("Vale kasutaja või parool");
        }
    }
}
