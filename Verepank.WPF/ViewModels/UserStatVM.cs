﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Verepank.Services;
using Verepank.Services.Interfaces;

namespace Verepank.WPF.ViewModels
{
    public class UserStatVM : BaseVM
    {
        //võtab doonori andmed baasist
        public void UserStats(string user, string pass, TextBox txtAmount, TextBox txtReesus, TextBox txtType)
        {
            IPersonService personService = new PersonService(new HumanDbContext());
            var persons = personService.GetAllPersons();
            var bloodSum = 0;
            foreach (var item in persons)
            {
                if (item.Firstname == user & item.Lastname == pass)
                {
                    txtType.Text = item.HumanBlood.Select(h => h.Blood.BloodType).FirstOrDefault().ToString();
                    txtReesus.Text = item.HumanBlood.Select(h => h.Blood.Reesus).FirstOrDefault().ToString();
                    bloodSum += item.HumanBlood.Select(h => h.Amount).FirstOrDefault();
                }
            }
            txtAmount.Text = bloodSum.ToString();

        }
    }
}
