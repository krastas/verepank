﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verepank.Domain;
using Verepank.Services;
using Verepank.Services.Interfaces;

using Verepank.Domain.Enums;

using Verepank.C.Migrations;
using System.Windows.Controls;

namespace Verepank.WPF.ViewModels
{
    //Statistikaakna vaatemudel
    public class StatisticksWindowVM : BaseVM
    {
        //Võtab andmebaasist kõik veretüübid ja reesused ning väljastab statistilise andmekogumi, palju igat tüüpi verd on
        public void Stats(TextBox txtBloodSum, TextBox txtRealBloodSum, TextBox txtABPluss, TextBox txtAPluss, TextBox txtBPluss,
            TextBox txtNullPluss, TextBox txtABMiinus, TextBox txtAMiinus, TextBox txtBMiinus, TextBox txtNullMiinus)
        {
            IPersonService personService = new PersonService(new HumanDbContext());
            var persons = personService.GetAllPersons();
            var removebloods = personService.GetAllRemovedBlood();
            var BloodSum = 0;
            var BloodRealSum = 0;
            var ABPlusBloodSum = 0;
            var APlusBloodSum = 0;
            var BPlusBloodSum = 0;
            var NullPlusBloodSum = 0;
            var ABMinusBloodSum = 0;
            var AMinusBloodSum = 0;
            var BMinusBloodSum = 0;
            var NullMinusBloodSum = 0;

            //foreach tsükkel liidab iga grupi verekogused kokku
            foreach (var item in persons)
            {
                BloodSum += item.HumanBlood.Select(h => h.Amount).FirstOrDefault();
                ABPlusBloodSum += item.HumanBlood.Where(h => h.Blood.Reesus.ToString().Equals("Pluss") && h.Blood.BloodType.ToString().Equals("AB")).Select(h => h.Amount).FirstOrDefault();
                APlusBloodSum += item.HumanBlood.Where(h => h.Blood.Reesus.ToString().Equals("Pluss") && h.Blood.BloodType.ToString().Equals("A")).Select(h => h.Amount).FirstOrDefault();
                BPlusBloodSum += item.HumanBlood.Where(h => h.Blood.Reesus.ToString().Equals("Pluss") && h.Blood.BloodType.ToString().Equals("B")).Select(h => h.Amount).FirstOrDefault();
                NullPlusBloodSum += item.HumanBlood.Where(h => h.Blood.Reesus.ToString().Equals("Pluss") && h.Blood.BloodType.ToString().Equals("Null")).Select(h => h.Amount).FirstOrDefault();
                ABMinusBloodSum += item.HumanBlood.Where(h => h.Blood.Reesus.ToString().Equals("Miinus") && h.Blood.BloodType.ToString().Equals("AB")).Select(h => h.Amount).FirstOrDefault();
                AMinusBloodSum += item.HumanBlood.Where(h => h.Blood.Reesus.ToString().Equals("Miinus") && h.Blood.BloodType.ToString().Equals("A")).Select(h => h.Amount).FirstOrDefault();
                BMinusBloodSum += item.HumanBlood.Where(h => h.Blood.Reesus.ToString().Equals("Miinus") && h.Blood.BloodType.ToString().Equals("B")).Select(h => h.Amount).FirstOrDefault();
                NullMinusBloodSum += item.HumanBlood.Where(h => h.Blood.Reesus.ToString().Equals("Miinus") && h.Blood.BloodType.ToString().Equals("Null")).Select(h => h.Amount).FirstOrDefault();
            }
            //Lahutatakse maha andmebaasist eemaldatud veri
            BloodRealSum = BloodSum - removebloods.Sum(h => h.Amount);
            ABPlusBloodSum -= removebloods.Where(x => x.BloodType.ToString().Equals("AB") && x.Reesus.ToString().Equals("Pluss")).Sum(x => x.Amount);
            APlusBloodSum -= removebloods.Where(x => x.BloodType.ToString().Equals("A") && x.Reesus.ToString().Equals("Pluss")).Sum(x => x.Amount);
            BPlusBloodSum -= removebloods.Where(x => x.BloodType.ToString().Equals("B") && x.Reesus.ToString().Equals("Pluss")).Sum(x => x.Amount);
            NullPlusBloodSum -= removebloods.Where(x => x.BloodType.ToString().Equals("Null") && x.Reesus.ToString().Equals("Pluss")).Sum(x => x.Amount);
            ABMinusBloodSum -= removebloods.Where(x => x.BloodType.ToString().Equals("AB") && x.Reesus.ToString().Equals("Miinus")).Sum(x => x.Amount);
            AMinusBloodSum -= removebloods.Where(x => x.BloodType.ToString().Equals("A") && x.Reesus.ToString().Equals("Miinus")).Sum(x => x.Amount);
            BMinusBloodSum -= removebloods.Where(x => x.BloodType.ToString().Equals("B") && x.Reesus.ToString().Equals("Miinus")).Sum(x => x.Amount);
            NullMinusBloodSum -= removebloods.Where(x => x.BloodType.ToString().Equals("Null") && x.Reesus.ToString().Equals("Miinus")).Sum(x => x.Amount);
            //Väljastatakse lõpptulemus
            txtBloodSum.Text = BloodSum.ToString();
            txtRealBloodSum.Text = BloodRealSum.ToString();
            txtABPluss.Text = ABPlusBloodSum.ToString();
            txtAPluss.Text = APlusBloodSum.ToString();
            txtBPluss.Text = BPlusBloodSum.ToString();
            txtNullPluss.Text = NullPlusBloodSum.ToString();
            txtABMiinus.Text = ABMinusBloodSum.ToString();
            txtAMiinus.Text = AMinusBloodSum.ToString();
            txtBMiinus.Text = BMinusBloodSum.ToString();
            txtNullMiinus.Text = NullMinusBloodSum.ToString();
        }
    }
}
