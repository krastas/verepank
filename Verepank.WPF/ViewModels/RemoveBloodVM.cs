﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Verepank.Domain;

namespace Verepank.WPF.ViewModels
{
    //Vere eemaldamise vaatemudel
    public class RemoveBloodVM : BaseVM
    {
        //Vere eemalduse andmete sisestus(Kogus,Reesus,Veretüüp)
        public void RemoveBlood(TextBox txtRemovedAmount, RadioButton txtReesusmiinus, ComboBoxItem txtnull, ComboBoxItem txtA, ComboBoxItem txtAB)
        {
            var db = new HumanDbContext();
            RemovedBlood newremoveBlood = new RemovedBlood();
            newremoveBlood.Amount = int.Parse(txtRemovedAmount.Text);
            if (txtReesusmiinus.IsChecked.Value)
            {
                newremoveBlood.Reesus = Domain.Enums.Reesus.Miinus;
            }
            else
            {
                newremoveBlood.Reesus = Domain.Enums.Reesus.Pluss;
            }
            if (txtnull.IsSelected)
            {
                newremoveBlood.BloodType = Domain.Enums.BloodType.Null;
            }
            else if (txtA.IsSelected)
            {
                newremoveBlood.BloodType = Domain.Enums.BloodType.A;
            }
            else if (txtAB.IsSelected)
            {
                newremoveBlood.BloodType = Domain.Enums.BloodType.AB;
            }
            else
            {
                newremoveBlood.BloodType = Domain.Enums.BloodType.B;
            }
            db.RemovedBlood.Add(newremoveBlood);
            db.SaveChanges();
        }

    }
}
