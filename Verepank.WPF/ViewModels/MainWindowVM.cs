﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Verepank.Domain;
using Verepank.Domain.Enums;

using Verepank.C.Migrations;
using Verepank.Services.Interfaces;
using Verepank.Services;
using System.Windows.Controls;
using System.Windows;
using log4net;
using System.Reflection;
using log4net.Config;
using System.Collections;

namespace Verepank.WPF.ViewModels
{
    //Peaakna vaatemudel
    public class MainWindowVM : BaseVM
    {
        private List<Person> _persons;

        public List<Person> Persons
        {
            get { return _persons; }
            private set
            {
                _persons = value;
                base.NotifyPropertyChanged("Persons");
            }
        }

        private IPersonService _personService;

        public MainWindowVM()
        {
            _persons = new List<Person>();
            _personService = new PersonService(new HumanDbContext());
        }
        //Laeb kõik andmebaasis olevad inimesed
        public void LoadData()
        {
            Persons = _personService.GetAllPersons();

        }


        //Otsing eesnime järgi
        public void FirstNameSearch(ListBox lboxData, TextBox txtFirstname)
        {
            _personService.SearchbyName(lboxData, txtFirstname);
        }
        //Otsing perekonnanime järgi
        public void LastNameSearch(ListBox lboxData, TextBox txtLastname)
        {
            _personService.SearchbyLast(lboxData, txtLastname);
        }

        //Andmete täitmine peaaknas
        public void FillData(ListBox lboxData, TextBox txtFirstname, TextBox txtLastname, TextBox txtEmail, TextBox txtPhone, ComboBox txtBlood,
            RadioButton txtReesuspluss, RadioButton txtReesusmiinus, Button btnAdd)
        {
            if (lboxData.SelectedIndex > -1) //täidab lahtrid andmetega
            {
                var selectedHuman = lboxData.SelectedItem as Person;

                if (selectedHuman != null)
                {
                    txtFirstname.Text = selectedHuman.Firstname;
                    txtLastname.Text = selectedHuman.Lastname;
                    txtEmail.Text = selectedHuman.Email;
                    txtPhone.Text = selectedHuman.Phone;
                    //txtAmount.Text = selectedHuman.HumanBlood.Select(Person => Person.Amount).FirstOrDefault().ToString();
                    txtBlood.Text = selectedHuman.HumanBlood.Select(Blood => Blood.Blood.BloodType).FirstOrDefault().ToString();
                    if (selectedHuman.HumanBlood.Select(Blood => Blood.Blood.Reesus).FirstOrDefault().ToString().Equals("Pluss"))
                    {
                        txtReesuspluss.IsChecked = true;
                    }
                    else
                    {
                        txtReesusmiinus.IsChecked = true;
                    }

                    btnAdd.Content = "Muuda";


                }
            }
        }
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        //Inimese lisamine andmebaasi
        public void AddNewPerson(ListBox lboxData, TextBox txtFirstname, TextBox txtLastname, TextBox txtEmail, TextBox txtPhone,
            RadioButton txtReesusmiinus, ComboBoxItem txtnull, ComboBoxItem txtA, ComboBoxItem txtAB, TextBox txtAmount, Button btnAdd)
        {
            var db = new HumanDbContext();
            if (lboxData.SelectedIndex == -1)
            {
                Person newPerson = new Person();
                newPerson.Firstname = txtFirstname.Text;
                newPerson.Lastname = txtLastname.Text;
                newPerson.Email = txtEmail.Text;
                newPerson.Phone = txtPhone.Text;

                Blood newBlood = new Blood();
                if (txtReesusmiinus.IsChecked.Value)
                {
                    newBlood.Reesus = Domain.Enums.Reesus.Miinus;
                }
                else
                {
                    newBlood.Reesus = Domain.Enums.Reesus.Pluss;
                }
                if (txtnull.IsSelected)
                {
                    newBlood.BloodType = Domain.Enums.BloodType.Null;
                }
                else if (txtA.IsSelected)
                {
                    newBlood.BloodType = Domain.Enums.BloodType.A;
                }
                else if (txtAB.IsSelected)
                {
                    newBlood.BloodType = Domain.Enums.BloodType.AB;
                }
                else
                {
                    newBlood.BloodType = Domain.Enums.BloodType.B;
                }

                HumanBlood newHumanBlood = new HumanBlood();
                newHumanBlood.Amount = int.Parse(txtAmount.Text);
                newHumanBlood.Person = newPerson;
                newHumanBlood.Blood = newBlood;

                db.HumanBlood.Add(newHumanBlood);
                db.Bloods.Add(newBlood);
                db.People.Add(newPerson);


                XmlConfigurator.Configure();
                _log.Info("Added new person");

            }
            else //muudab inimese andmeid
            {
                if (lboxData.SelectedIndex > -1)
                {
                    var selectedHuman = lboxData.SelectedItem as Person;
                    var selectedBlood = selectedHuman.HumanBlood.Select(h => h.PersonId).FirstOrDefault();
                    if (selectedHuman.PersonId == selectedBlood)
                    {
                        HumanDbContext dbContext = new HumanDbContext();
                        HumanBlood humanblood = dbContext.HumanBlood.First(p => p.PersonId == selectedBlood);

                        humanblood.Amount = int.Parse(txtAmount.Text);
                        dbContext.SaveChanges();
                    }
                    btnAdd.Content = "Lisa inimene";
                    lboxData.SelectedIndex = -1;
                }
            }

            lboxData.Items.Refresh();
            db.SaveChanges();
        }

    }
}
