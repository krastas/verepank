﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Verepank.WPF.ViewModels;

namespace Verepank.WPF
{
    /// <summary>
    /// Interaction logic for UserStat.xaml
    /// </summary>
    public partial class UserStat : Window
    {
        private UserStatVM _vm;
        
        public UserStat(string user, string pass)
        {
            InitializeComponent();
            _vm = new UserStatVM();
            _vm.UserStats(user, pass, txtAmount, txtReesus, txtType);
            this.DataContext = _vm;
        }
    }
}
