﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Verepank.Domain;
using Verepank.WPF.ViewModels;

namespace Verepank.WPF
{
    /// <summary>
    /// Interaction logic for RemoveBlood.xaml
    /// </summary>
    public partial class RemoveBlood : Window
    {
        private RemoveBloodVM _vm;
        public RemoveBlood()
        {
            InitializeComponent();
        }
        //Nupu vajutamisel läheb peaaknasse
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            App.Current.MainWindow = main;
            this.Close();
            main.Show();
        }
        //Nupu vajutamisel läheb statistikaaknasse
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            StatisticksWindow statisticks = new StatisticksWindow();
            App.Current.MainWindow = statisticks;
            this.Close();
            statisticks.Show();
        }
        //Nupu vajutamisel eemaldatakse veri andmebaasist
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            _vm = new RemoveBloodVM();
            _vm.RemoveBlood(txtRemovedAmount, txtReesusmiinus, txtnull, txtA, txtAB);
        }
    }
}
