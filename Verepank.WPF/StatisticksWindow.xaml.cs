﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Verepank.Domain;
using Verepank.Services;
using Verepank.Services.Interfaces;
using Verepank.WPF.ViewModels;

namespace Verepank.WPF
{
    /// <summary>
    /// Interaction logic for StatisticksWindow.xaml
    /// </summary>
    public partial class StatisticksWindow : Window
    {
        private StatisticksWindowVM _vm;
        public StatisticksWindow()
        {
            InitializeComponent();
            this.Loaded += StatisticksWindow_Loaded;
        }
        //Statistikaakna laadimine
        private void StatisticksWindow_Loaded(object sender, RoutedEventArgs e)
        {
            _vm = new StatisticksWindowVM();
            _vm.Stats(txtBloodSum, txtRealBloodSum, txtABPluss, txtAPluss, txtBPluss, txtNullPluss, txtABMiinus, txtAMiinus, txtBMiinus, txtNullMiinus);
            this.DataContext = _vm;
        }
        //Nupu vajutamisele läheb peaaknasse ehk inimese lisamise ja valimise juurde
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            App.Current.MainWindow = main;
            this.Close();
            main.Show();
        }
        //Nupu vajutamisel läheb vere eemaldamise aknasse
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            RemoveBlood rm = new RemoveBlood();
            App.Current.MainWindow = rm;
            this.Close();
            rm.Show();
        }
    }
}
