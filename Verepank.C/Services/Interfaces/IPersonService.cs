﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Verepank.Domain;

namespace Verepank.Services.Interfaces
{
    //Interfeiss Inimese ja kustutatud vere jaoks
    public interface IPersonService
    {
        List<Person> GetAllPersons();
        List<RemovedBlood> GetAllRemovedBlood();
        void SearchbyName(ListBox lboxData, TextBox Firstname);
        void SearchbyLast(ListBox lboxData, TextBox Lastname);
    }
}
