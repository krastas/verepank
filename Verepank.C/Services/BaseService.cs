﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Verepank.Services
{
    //Baasservice
    public class BaseService
    {
        protected HumanDbContext DataContext;

        public BaseService(HumanDbContext ctx)
        {
            DataContext = ctx;
        }
    }
}
