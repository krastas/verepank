﻿using Verepank.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verepank.Domain;
using System.Windows.Controls;

namespace Verepank.Services
{
    public class PersonService : BaseService, IPersonService
    {
        public PersonService(HumanDbContext dbContext) : base(dbContext)
        {

        }
        //Loob listi kõigi inimeste jaoks
        public List<Person> GetAllPersons()
        {
            return base.DataContext.People.ToList();
        }
        //Loob listi kõigi eemaldatud vere jaoks
        public List<RemovedBlood> GetAllRemovedBlood()
        {
            return base.DataContext.RemovedBlood.ToList();
        }
        public void SearchbyName(ListBox lboxData, TextBox Firstname)
        {
            string txtOrig = Firstname.Text;
            string upper = txtOrig.ToUpper();
            string lower = txtOrig.ToLower();


            var persons = GetAllPersons();

            var fnameFiltered = from Per in persons
                                let ename = Per.Firstname
                                where
                                 ename.StartsWith(lower)
                                 || ename.StartsWith(upper)
                                 || ename.Contains(txtOrig)
                                select Per;
            lboxData.ItemsSource = fnameFiltered;
        }
        public void SearchbyLast(ListBox lboxData, TextBox Lastname)
        {
            string txtOrig = Lastname.Text;
            string upper = txtOrig.ToUpper();
            string lower = txtOrig.ToLower();


            var persons = GetAllPersons();

            var lnameFiltered = from Per in persons
                                let lname = Per.Lastname
                                where
                                 lname.StartsWith(lower)
                                 || lname.StartsWith(upper)
                                 || lname.Contains(txtOrig)
                                select Per;
            lboxData.ItemsSource = lnameFiltered;
        }
    }
}
