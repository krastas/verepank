﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verepank.Domain.Enums;

namespace Verepank.Domain
{
    //D9meen Vere märgistamise jaoks
    public class Blood
    {
        public int BloodId { get; set; }
        public BloodType BloodType { get; set; }
        public Reesus Reesus { get; set; }

        public virtual List<HumanBlood> HumanBlood { get; set; }
    }
}
