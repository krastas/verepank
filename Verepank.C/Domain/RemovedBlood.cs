﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verepank.Domain;
using Verepank.Domain.Enums;

namespace Verepank.Domain
{
    //Domeen vere eemaldamise jaoks
    public class RemovedBlood
    {
        public int RemovedBloodId { get; set; }
        public BloodType BloodType { get; set; }
        public Reesus Reesus { get; set; }
        public int Amount { get; set; }

    }
}
