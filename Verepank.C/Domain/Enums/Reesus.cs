﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Verepank.Domain.Enums
{
    //Enumid vere reesuse jaoks
    public enum Reesus
    {
        Pluss = 1,
        Miinus = 2
    }
}
