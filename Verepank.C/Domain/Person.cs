﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verepank.Domain.Enums;

namespace Verepank.Domain
{
    //Domeen Inimese jaoks
    public class Person
    {
        public int PersonId { get; set; }

        [Required]
        [MaxLength(length: 80)]
        public string Firstname { get; set; }
        [Required]
        [MaxLength(length: 100)]
        public string Lastname { get; set; }
        [Required]
        [MaxLength(length: 100)]
        public string Phone { get; set; }
        [Required]
        [MaxLength(length: 100)]
        public string Email { get; set; }

        public virtual List<HumanBlood> HumanBlood { get; set; }
    }
}
