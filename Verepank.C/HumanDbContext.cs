﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verepank.Domain;

namespace Verepank
{
    //Võtab ja lisab andmed tabelisse
    public class HumanDbContext : DbContext
    {
        public DbSet<Person> People { get; set; }
        public DbSet<Blood> Bloods { get; set; }
        public DbSet<HumanBlood> HumanBlood { get; set; }
        public DbSet<RemovedBlood> RemovedBlood { get; set; }

        public HumanDbContext() : base("name=HumanDbSql")
        {

        }
    }
}
