namespace Verepank.C.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bloods",
                c => new
                    {
                        BloodId = c.Int(nullable: false, identity: true),
                        BloodType = c.Int(nullable: false),
                        Reesus = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BloodId);
            
            CreateTable(
                "dbo.HumanBloods",
                c => new
                    {
                        HumanBloodId = c.Int(nullable: false, identity: true),
                        PersonId = c.Int(nullable: false),
                        Amount = c.Int(nullable: false),
                        Blood_BloodId = c.Int(),
                    })
                .PrimaryKey(t => t.HumanBloodId)
                .ForeignKey("dbo.Bloods", t => t.Blood_BloodId)
                .ForeignKey("dbo.People", t => t.PersonId, cascadeDelete: true)
                .Index(t => t.PersonId)
                .Index(t => t.Blood_BloodId);
            
            CreateTable(
                "dbo.People",
                c => new
                    {
                        PersonId = c.Int(nullable: false, identity: true),
                        Firstname = c.String(nullable: false, maxLength: 80),
                        Lastname = c.String(nullable: false, maxLength: 100),
                        Phone = c.String(nullable: false, maxLength: 100),
                        Email = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.PersonId);

            CreateTable(
                "dbo.RemovedBloods",
                c => new
                {
                    RemovedBloodId = c.Int(nullable: false, identity: true),
                    BloodType = c.Int(nullable: false),
                    Reesus = c.Int(nullable: false),
                    Amount = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.RemovedBloodId);

        }
        
        public override void Down()
        {
            DropForeignKey("dbo.HumanBloods", "PersonId", "dbo.People");
            DropForeignKey("dbo.HumanBloods", "Blood_BloodId", "dbo.Bloods");
            DropIndex("dbo.HumanBloods", new[] { "Blood_BloodId" });
            DropIndex("dbo.HumanBloods", new[] { "PersonId" });
            DropTable("dbo.People");
            DropTable("dbo.HumanBloods");
            DropTable("dbo.Bloods");
            DropTable("dbo.RemovedBloods");
        }
    }
}
